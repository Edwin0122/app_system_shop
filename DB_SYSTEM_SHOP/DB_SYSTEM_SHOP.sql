CREATE DATABASE DB_SYSTEM_SHOP;
GO
USE DB_SYSTEM_SHOP;
GO
--Tabla Productos
CREATE TABLE products (
idProducts INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
code			VARCHAR (10) NOT NULL,
nameProduct		VARCHAR (50) NOT NULL,
descriptions	VARCHAR (100) NOT NULL,
presentation	VARCHAR (10) NOT NULL,
unitCost		DECIMAL (12,2) NOT NULL,
salePrice		DECIMAL (12,2) NOT NULL,
chargeType		VARCHAR (10) NOT NULL,
);
GO

--Tabla Inventario
CREATE TABLE inventory (
idInventory INT  NOT NULL,
code			VARCHAR (10) NOT NULL,
nameProduct		VARCHAR (50) NOT NULL,
quantity		INT NOT NULL,
unitCost		DECIMAL (12,2) NOT NULL,
salePrice		DECIMAL (12,2) NOT NULL,
totalMount		DECIMAL (12,2) NOT NULL,
chargeType		VARCHAR (10) NOT NULL,
);
GO

----------------------Procedimientos Almacenados-----------------------
-----------------------------------------------------------------------

--Agregar Producto
CREATE PROC addProduct
@code			VARCHAR (10),
@nameProduct		VARCHAR (50),
@descriptions	VARCHAR (100),
@presentation	VARCHAR (10),
@unitCost		DECIMAL (12,2),
@salePrice		DECIMAL (12,2),
@chargeType		VARCHAR (10)
AS
INSERT INTO products (code, nameProduct, descriptions, presentation, unitCost, salePrice, chargeType)
VALUES(@code, @nameProduct, @descriptions, @presentation, @unitCost, @salePrice, @chargeType)
GO

--Editar Producto
CREATE PROC editProduct
@idProducts INT,
@code			VARCHAR (10),
@nameProduct		VARCHAR (50),
@descriptions	VARCHAR (100),
@presentation	VARCHAR (10),
@unitCost		DECIMAL (12,2),
@salePrice		DECIMAL (12,2),
@chargeType		VARCHAR (10)
AS
UPDATE products SET code=@code, nameProduct=@nameProduct, descriptions=@descriptions, presentation=@presentation, 
unitCost=@unitCost, salePrice=@salePrice, chargeType=@chargeType WHERE idProducts=@idProducts
GO

--Eliminar Producto
CREATE PROC deleteProduct
@idProducts INT
AS
DELETE FROM products WHERE idProducts=@idProducts
GO

--------------------Trigger para agregar un producto al inventario-------------------------
-------------------------------------------------------------------------------------------

CREATE TRIGGER trAddProductInventory
ON products FOR INSERT
AS
DECLARE @idInventory INT
DECLARE @code VARCHAR (10)
DECLARE @nameProduct VARCHAR (50)
DECLARE @quantity INT
DECLARE @unitCost DECIMAL (12,2)
DECLARE @salePrice DECIMAL (12,2)
DECLARE @totalMount DECIMAL (12,2)
DECLARE @chargeType VARCHAR (10)
SELECT @idInventory=idProducts, @code=code, @nameProduct=nameProduct, @quantity=0, @unitCost=unitCost,
@salePrice=salePrice, @totalMount=(@quantity*@unitCost), @chargeType=chargeType FROM inserted
INSERT INTO inventory (idInventory, code, nameProduct, quantity, unitCost, salePrice, totalMount, chargeType)
VALUES (@idInventory, @code, @nameProduct, @quantity, @unitCost, @salePrice, @totalMount, @chargeType)
GO