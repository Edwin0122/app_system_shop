﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataLayer
{
    public class CdConexion
    {
        //Coneccion a mi base de datos desde el archivo App.config
        private SqlConnection Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString);

        //Metodo que permite abrir la conexion a la base de datos
        public SqlConnection OpenDb()
        {
            if (Conexion.State == ConnectionState.Closed)
            {
                Conexion.Open();
            }
            return Conexion;
        }

        //Metodo que permite cerrar la conexion a la base de datos
        public SqlConnection CloseDb()
        {
            if (Conexion.State == ConnectionState.Open)
            {
                Conexion.Close();
            }
            return Conexion;
        }

    }
}
