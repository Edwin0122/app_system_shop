﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace DataLayer
{
    public class Cd_Procedure
    {
        CdConexion Con = new CdConexion();
        SqlCommand sqlComand;
        SqlDataReader dataReader;
        DataTable dataTable;

        //Método que me permite cargar los datos de una tabla a un DataGridView
        public DataTable loadData(string Table)
        {
            dataTable = new DataTable("Load_Data");
            sqlComand = new SqlCommand("Select * From "+Table, Con.OpenDb());
            sqlComand.CommandType = CommandType.Text;

            dataReader = sqlComand.ExecuteReader();
            dataTable.Load(dataReader);
            dataReader.Close();

            Con.CloseDb();

            return dataTable;

        }


        //Método que me permite Alterar los colores de las filas de una DataGridView
        public void toggleRowColorDataGridView(DataGridView dataGridView)
        {
            dataGridView.AlternatingRowsDefaultCellStyle.BackColor = Color.LightBlue;
            dataGridView.DefaultCellStyle.BackColor = Color.White;

        }


        //Método que me permite generar los codigos (Prefijos) de los clientes, proveedores, etc
        public string generateCode(string Table)
        {
            string code = string.Empty;
            int total = 0;
            sqlComand = new SqlCommand("Select (*) as totalRecords From" + Table, Con.OpenDb());
            sqlComand.CommandType = CommandType.Text;

            dataReader = sqlComand.ExecuteReader();

            if (dataReader.Read())
            {
                total = Convert.ToInt32(dataReader["totalRecords"]) + 1;
            }
            dataReader.Close();

            if (total < 10)
            {
                code = "0000000" + total;
            }
            else if (total < 100)
            {
                code = "000000" + total;
            }
            else if (total < 1000)
            {
                code = "00000" + total;
            }
            else if (total < 10000)
            {
                code = "0000" + total;
            }
            else if (total < 100000)
            {
                code = "000" + total;
            }
            else if (total < 1000000)
            {
                code = "00" + total;
            }
            else if (total < 10000000)
            {
                code = "0" + total;
            }


            Con.CloseDb();
            return code;

        }


        //Método que me permite generar los codigos (ID) de los clientes, proveedores, etc
        public string generateCodeId(string Table)
        {
            string code = string.Empty;
            int total = 0;
            sqlComand = new SqlCommand("Select (*) as totalRecords From" + Table, Con.OpenDb());
            sqlComand.CommandType = CommandType.Text;

            dataReader = sqlComand.ExecuteReader();

            if (dataReader.Read())
            {
                total = Convert.ToInt32(dataReader["totalRecords"]) + 1;
            }
            dataReader.Close();

            Con.CloseDb();
            return code;

        }


        //Método que permite dar formato moneda a un TextBox o caja de texto
        public void currencyFormat(TextBox textBox)
        {
            if (textBox.Text == string.Empty)
            {
                return;
            }
            else
            {
                decimal amount;
                amount = Convert.ToDecimal(textBox.Text);
                textBox.Text=amount.ToString("N2");
            }
        }


        //Método que permite limpiar la caaja de texto que encuentre en el formulario
        public void cleanControl(Form xForm)
        {
            foreach (var xcontrol in xForm.Controls)
            {
                if (xcontrol is TextBox)
                {
                    ((TextBox)xcontrol).Text = string.Empty;
                }
                else if (xcontrol is ComboBox)
                {
                    ((ComboBox)xcontrol).Text = string.Empty; 
                }
            }
        }


        //Método que permite llenar la caaja de texto que se encuentra en el formulario
        public void fillComboBox(string table, string name, ComboBox xcomboBox)
        {
            sqlComand = new SqlCommand("Select * From " + table, Con.OpenDb());
            sqlComand.CommandType = CommandType.Text;
            dataReader = sqlComand.ExecuteReader();

            while (dataReader.Read())
            {
                xcomboBox.Items.Add(dataReader[name].ToString());
            }
        }
    }
}
