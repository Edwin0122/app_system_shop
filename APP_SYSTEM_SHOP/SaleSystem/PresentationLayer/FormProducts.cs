﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DomainLayer;
using FeatureLayer;

namespace PresentationLayer
{
    public partial class FormProducts : FormBase
    {
        public FormProducts()
        {
            InitializeComponent();
        }

        Cdo_procedures ObjProcedures = new Cdo_procedures();

        private void FormProducts_Load(object sender, EventArgs e)
        {
            CargarData();
        }

        private void CargarData()
        {
            dataGridView1.DataSource = ObjProcedures.loadData("products");
            dataGridView1.ClearSelection();
        }
    }
}
