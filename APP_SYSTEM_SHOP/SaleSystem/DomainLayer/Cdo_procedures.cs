﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using DataLayer;

namespace DomainLayer
{
    public class Cdo_procedures
    {
        Cd_Procedure ObjProcedures = new Cd_Procedure();

        //Método que me permite cargar los datos de una tabla a un DataGridView
        public DataTable loadData(string Table)
        {
            return ObjProcedures.loadData(Table);
        }


        //Método que me permite Alterar los colores de las filas de una DataGridView
        public void toggleRowColorDataGridView(DataGridView dataGridView)
        {
            ObjProcedures.toggleRowColorDataGridView(dataGridView);

        }


        //Método que me permite generar los codigos (Prefijos) de los clientes, proveedores, etc
        public string generateCode(string Table)
        {
            return ObjProcedures.generateCode(Table);
        }


        //Método que me permite generar los codigos (ID) de los clientes, proveedores, etc
        public string generateCodeId(string Table)
        {
           return ObjProcedures.generateCodeId(Table);

        }


        //Método que permite dar formato moneda a un TextBox o caja de texto
        public void currencyFormat(TextBox textBox)
        {
            ObjProcedures.currencyFormat(textBox);
        }


        //Método que permite limpiar la caaja de texto que encuentre en el formulario
        public void cleanControl(Form xForm)
        {
            ObjProcedures.cleanControl(xForm);
        }


        //Método que permite llenar la caaja de texto que se encuentra en el formulario
        public void fillComboBox(string table, string name, ComboBox xcomboBox)
        {
           ObjProcedures.fillComboBox(table, name, xcomboBox);
        }
    }
}
